This is a [GoNextCRud](https://gitlab.com/vipinkumar021193/go-nextjs-crud) using [NextJs](https://nextjs.org/), [TypeScript](https://www.typescriptlang.org/),  [Tailwind](https://tailwindcss.com/)[Golang](https://go.dev/), [Dcoker](https://www.docker.com/), [Postgresql](https://www.postgresql.org/)

## Getting Started

First, clone project and setup environment variables:

```bash
git clone git@gitlab.com:vipinkumar021193/go-nextjs-crud.git
cd go-nextjs-crud
cp currentdire/nextapp/.env.example  currentdire/nextapp/.env
cp currentdire/api/.env.example  currentdire/api/.env
```


## Run Project with Docker

```bash
cd go-nextjs-crud
docker-compose up
```

[Open frontend](loclahost:3000)
[Open backend](loclahost:3001) 

## Run Project without Docker

### Run frontend
```bash
cd go-nextjs-crud/nextapp
yarn 
yarn dev
or 
npm install
npm run dev
```

### Run backend
```bash
cd go-nextjs-crud/api
go tidy
go run cmd/main.go
```

[Open frontend](loclahost:3000)
[Open backend](loclahost:3001)
