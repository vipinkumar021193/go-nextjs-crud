package main

import (
	"fmt"
	"log"
	"os"

	"github.com/gofiber/fiber/v2"
	"github.com/joho/godotenv"
	"gitlab.com/goapi/algo"
	"gitlab.com/goapi/pkg/database"
	"gitlab.com/goapi/pkg/routes"
)

func init() {
	err := godotenv.Load()
	if err != nil {
		log.Fatalln("Error loading .env file", err.Error())
	}
}

func main() {
	fmt.Println(os.Getenv("DATABASE_URL"))
	algo.Algo()
	DB := database.Init()

	app := fiber.New()

	routes.Routes(DB, app)

	addr := ":" + os.Getenv("PORT")
	log.Println("dev running server on " + addr)
	app.Listen(addr)
}
