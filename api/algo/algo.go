package algo

import (
	"fmt"
	"sort"
	"strings"
)

type wordStruct struct {
	word string
	val  int
}

func anothergetMostReaptedWord(words []string, char string) []string {
	wordsWithCountMap := make(map[string]int)
	for _, v := range words {
		wordsWithCountMap[v] = strings.Count(v, char)
	}

	var wordsWithCountStruct []wordStruct
	for i, v := range wordsWithCountMap {
		g := wordStruct{i, v}
		wordsWithCountStruct = append(wordsWithCountStruct, g)
	}

	sort.Slice(wordsWithCountStruct, func(i, j int) bool {
		if wordsWithCountStruct[i].val != wordsWithCountStruct[j].val {
			return wordsWithCountStruct[i].val > wordsWithCountStruct[j].val
		}
		return len(wordsWithCountStruct[i].word) > len(wordsWithCountStruct[j].word)
	})

	sortedWords := make([]string, 0, len(words))
	for _, v := range wordsWithCountStruct {
		sortedWords = append(sortedWords, v.word)
	}

	return sortedWords
}

func getSortedMostReaptedWordsList(words []string, char string) []string {

	sort.Slice(words, func(i, j int) bool {
		stringI := words[i]
		stringJ := words[j]

		stringCountI := strings.Count(stringI, char)
		stringCountJ := strings.Count(stringJ, char)

		if stringCountI != stringCountJ {
			return stringCountI > stringCountJ
		}
		return len(stringI) > len(stringJ)
	})

	return words
}

func getMostReaptedWord(words []string) string {

	wordsWithCountMap := make(map[string]int)
	for _, v := range words {
		if val, ok := wordsWithCountMap[v]; ok {
			wordsWithCountMap[v] = val + 1
		} else {
			wordsWithCountMap[v] = 1
		}
	}

	keys := make([]string, 0, len(wordsWithCountMap))

	for key := range wordsWithCountMap {
		keys = append(keys, key)
	}

	sort.Slice(keys, func(i, j int) bool {
		return wordsWithCountMap[keys[i]] > wordsWithCountMap[keys[j]]
	})

	return keys[0]
}

func IntegerValuePrint(num int) {
	if num < 3 {
		fmt.Println(2)
	} else {
		// IntegerValuePrint(num * 1)
	}

}

func Algo() {

	var strings = []string{"aaaasd", "a", "ab", "aab", "aaabcd", "ef", "cssssssd", "fdz", "kf",
		"zc", "lklklklklklklklkl", "l"}

	var slices = []string{"apple", "pie", "apple", "red", "red", "red"}

	fmt.Println(getSortedMostReaptedWordsList(strings, "a"))
	fmt.Println(anothergetMostReaptedWord(strings, "a"))
	fmt.Println(getMostReaptedWord(slices))
	IntegerValuePrint(10)
}
