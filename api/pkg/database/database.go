package database

import (
	"log"
	"os"

	"gitlab.com/goapi/pkg/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func Init() *gorm.DB {

	dbURL := os.Getenv("DATABASE_URL")
	db, err := gorm.Open(postgres.Open(dbURL), &gorm.Config{})

	// Auto Migrate
	db.AutoMigrate(&models.User{})

	if err != nil {
		log.Fatalln(err)
	}

	return db
}
