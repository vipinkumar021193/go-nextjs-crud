package helpers

import (
	"net/mail"
	"strconv"

	"gitlab.com/goapi/pkg/models"
	"gorm.io/gorm"
)

func StringToInt(value string) uint {
	u64, err := strconv.ParseUint(value, 10, 32)
	if err != nil {
		return 0
	}
	wd := uint(u64)
	return wd
}

func EamilValid(email string) bool {
	_, err := mail.ParseAddress(email)
	return err == nil
}

func CreateUserValidation(user models.User) []map[string]string {
	errorMessages := make([]map[string]string, 0, 2)

	if user.Name == "" {
		errorMessages = append(errorMessages, map[string]string{"name": "Name required"})
	}

	if user.Email == "" {
		errorMessages = append(errorMessages, map[string]string{"email": "Email required"})
	}

	if user.Email != "" {
		isValidEmail := EamilValid(user.Email)
		if !isValidEmail {
			errorMessages = append(errorMessages, map[string]string{"email": "Email is not valid"})
		}
	}

	return errorMessages
}

func UpdateUserValidation(DB *gorm.DB, user models.User, userRequest models.User) []map[string]string {
	errorMessages := make([]map[string]string, 0, 2)

	if userRequest.Name == "" {
		errorMessages = append(errorMessages, map[string]string{"name": "Name required"})
	}

	if userRequest.Email != "" {
		isValidEmail := EamilValid(userRequest.Email)
		if !isValidEmail {
			errorMessages = append(errorMessages, map[string]string{"email": "Email is not valid"})
		}
	}

	if userRequest.Email != "" {
		oldUser, err := models.GetUserByEmail(DB, userRequest.Email)
		if err == nil && oldUser.Email != user.Email {
			errorMessages = append(errorMessages, map[string]string{"email": "Email already taken"})
		}
	}

	return errorMessages
}
