package routes

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/goapi/pkg/controllers"
	"gorm.io/gorm"
)

// Routes is a wrapper that all routes defind
func Routes(DB *gorm.DB, app *fiber.App) {
	h := controllers.New(DB)
	api := app.Group("/api")
	v1 := api.Group("/v1")
	v1.Get("/", h.Index)
	v1.Get("/users", h.GetAllUsers)
	v1.Get("/user/:id", h.GetUser)
	v1.Post("/create/user", h.CreateUser)
	v1.Patch("/update/user/:id", h.UpdateUser)
	v1.Delete("/delete/user/:id", h.DeleteUser)
}
