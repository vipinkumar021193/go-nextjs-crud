package controllers

import (
	"encoding/json"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/goapi/pkg/helpers"
	"gitlab.com/goapi/pkg/models"
)

func (h handler) Index(c *fiber.Ctx) error {
	return c.JSON(fiber.Map{"message": "hello"})
}

func (h handler) GetAllUsers(c *fiber.Ctx) error {

	users, err := models.GetAllUsers(h.DB)
	if err != nil {
		return c.JSON(fiber.Map{"message": err.Error()})
	}

	json.Marshal(users)
	return c.JSON(users)
}

func (h handler) GetUser(c *fiber.Ctx) error {

	userId := helpers.StringToInt(c.Params("id"))
	user, err := models.GetUserById(h.DB, userId)
	if err != nil {
		c.Status(400)
		return c.JSON(fiber.Map{"message": err.Error()})
	}
	json.Marshal(user)
	return c.JSON(user)
}

func (h handler) CreateUser(c *fiber.Ctx) error {

	params := new(models.User)
	c.BodyParser(&params)
	user := models.User{
		Name:  params.Name,
		Email: params.Email,
	}

	errorMessages := helpers.CreateUserValidation(user)

	if len(errorMessages) > 0 {
		c.Status(400)
		return c.JSON(fiber.Map{"errors": errorMessages})
	}

	err := models.CreateUser(h.DB, user)

	if err != nil {
		c.Status(500)
		return c.JSON(fiber.Map{"message": "server error"})
	}

	c.Status(201)
	return c.JSON(fiber.Map{"message": "success"})
}

func (h handler) UpdateUser(c *fiber.Ctx) error {

	params := new(models.User)
	c.BodyParser(&params)
	userRequest := models.User{
		Name:  params.Name,
		Email: params.Email,
	}

	userId := helpers.StringToInt(c.Params("id"))
	user, err := models.GetUserById(h.DB, userId)
	if err != nil {
		c.Status(400)
		return c.JSON(fiber.Map{"message": err.Error()})
	}

	errorMessages := helpers.UpdateUserValidation(h.DB, user, userRequest)

	if len(errorMessages) > 0 {
		c.Status(400)
		return c.JSON(fiber.Map{"errors": errorMessages})
	}

	code, err := models.UpdateUserById(h.DB, user, userRequest)
	if err != nil {
		c.Status(code)
		return c.JSON(fiber.Map{"message": err.Error()})
	}

	c.Status(200)
	return c.JSON(fiber.Map{"message": "success"})

}

func (h handler) DeleteUser(c *fiber.Ctx) error {
	userId := helpers.StringToInt(c.Params("id"))
	code, err := models.DeleteUserById(h.DB, userId)
	if err != nil {
		c.Status(code)
		return c.JSON(fiber.Map{"message": err.Error()})
	}
	return c.JSON(fiber.Map{"message": "success"})
}
