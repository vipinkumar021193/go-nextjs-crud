package models

import (
	"gorm.io/gorm"
)

type User struct {
	Name  string `json:"name"`
	Email string `json:"email"`
	gorm.Model
}

func GetAllUsers(DB *gorm.DB) ([]User, error) {
	var users []User

	result := DB.Find(&users)
	if result.Error != nil {
		return users, result.Error
	}
	return users, nil
}

func GetUserById(DB *gorm.DB, userId uint) (User, error) {
	var user User

	result := DB.First(&user, userId)
	if result.Error != nil {
		return user, result.Error
	}
	return user, nil
}

func GetUserByEmail(DB *gorm.DB, email string) (User, error) {
	var user User
	result := DB.Where(&User{Email: email}).First(&user)
	if result.Error != nil {
		return user, result.Error
	}
	return user, nil
}

func CreateUser(DB *gorm.DB, user User) error {
	if result := DB.Create(&user); result.Error != nil {
		return result.Error
	}
	return nil
}

func UpdateUserById(DB *gorm.DB, user User, userRequest User) (int, error) {

	user.Name = userRequest.Name
	if userRequest.Email != "" {
		user.Email = userRequest.Email
	}

	if result := DB.Save(&user); result.Error != nil {
		return 500, result.Error
	}
	return 200, nil
}

func DeleteUserById(DB *gorm.DB, userId uint) (int, error) {
	user, err := GetUserById(DB, userId)
	if err != nil {
		return 400, err
	}

	if result := DB.Delete(&user); result.Error != nil {
		return 500, result.Error
	}
	return 200, nil
}
